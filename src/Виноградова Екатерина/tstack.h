#ifndef __STACK_H__
#define __STACK_H__

#include "tdataroot.h"

class TStack : public TDataRoot
{
protected:
	int Hi;
	virtual int GetNextIndex(int index);
public:
	TStack(int Size = DefMemSize) : TDataRoot(Size), Hi(-1) { }
	TStack(const TStack &);
	void Put(const TData &);
	TData Get();
	int  IsValid();
	void Print();
	TData GetTopElem();
};

#endif